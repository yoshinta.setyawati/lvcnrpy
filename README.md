# lvcnrpy

`lvcnrpy` is a simple Python interface to the
[`lvcnr-lfs`](https://git.ligo.org/waveforms/lvcnr-lfs) numerical relativity
repository.

## Installation

1.  **Install the package locally**

    The package can be installed directly from
    [`git.ligo.org`](https://git.ligo.org/) using the following snippet

    ```bash
    $ pip install git+https://git.ligo.org/waveforms/lvcnrpy.git
    ```

2.  **Define [`lvcnr-lfs`](https://git.ligo.org/waveforms/lvcnr-lfs) repository environment variable**

    To use [`lvcnrjson`](#generating-a-json-dump-of-all-lvcnr-lfs-attributes)
    you must define the environment variable `LVCNR_DATADIR` as the path to
    a local clone of the
    [`lvcnr-lfs`](https://git.ligo.org/waveforms/lvcnr-lfs) repository. If you
    are using a virtual environment this can be defined in your activation
    script. For a bash shell this could be done with

    ```bash
    export LVCNR_DATADIR=<path-to-lvc-nr-repo>
    ```
    
    If you do not want to use `lvcnrjson` then you can set the environment
    variable to an empty sting. For a bash shell this could be done with
    
    ```bash
    export LVCNR_DATADIR=""
    ```

## Usage

### Checking HDF5 NR files against format specifications

A working draft of the possible formats HDF5 NR files may be committed in to
the [`lvcnr-lfs`](https://git.ligo.org/waveforms/lvcnr-lfs) repository is
avilable at
[Numerical Relativity Waveforms](https://www.lsc-group.phys.uwm.edu/ligovirgo/cbcnote/Waveforms/NR#BBH_Waveform_Catalog:_Metadata).
To check a HDF5 NR file against one of the format levels use the included
program `lvcnrcheck`. It is installed as a global program when this package is
installed.

-   **`lvcnrcheck` usage**

    ```bash
    lvcnrcheck [-h] [-c] [-f {1,2,3}] file

    Check hdf5 NR data meet LVCNR format specifications

    positional arguments:
      file                  name of file to check

    optional arguments:
      -h, --help            show this help message and exit
      -c, --col             use coloured output (default: False)
      -f {1,2,3}, --format {1,2,3}
                            format level to use (default: 1)
    ```

-   **`lvcnrcheck` example**

    For basic usage simple pass the file name to `lvcnrcheck`

    ```bash
    $ cd <path-to-lvc-nr-repo>
    $ lvcnrcheck ./Cardiff-UIB/GW150914-followup/q1.2_base_96.h5
    ```

    To check against a specific format level use the `-f` option

    ```bash
    $ cd <path-to-lvc-nr-repo>
    $ lvcnrcheck -f 2 ./Cardiff-UIB/GW150914-followup/q1.2_base_96.h5
    ```

    To add a coloured output use the `-c` flag

    ```bash
    $ cd <path-to-lvc-nr-repo>
    $ lvcnrcheck -c ./Cardiff-UIB/GW150914-followup/q1.2_base_96.h5
    ```
    
### Generating a JSON dump of all [`lvcnr-lfs`](https://git.ligo.org/waveforms/lvcnr-lfs) attributes

To generate a JSON file containing all format level 1 attributes for all
simulations in the [`lvcnr-lfs`](https://git.ligo.org/waveforms/lvcnr-lfs)
repository you can use `lvcnrjson`. This requires that the environment
variable `LVCNR_DATADIR` has been set to the location of a local clone of
[`lvcnr-lfs`](https://git.ligo.org/waveforms/lvcnr-lfs) repository.

-   **`lvcnrjson` usage**
    
    ```bash
    lvcnrjson [-h]

    Export format level 1 metadata for all simulations to JSON

    optional arguments:
      -h, --help  show this help message and exit
    ```

