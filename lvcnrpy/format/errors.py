# Copyright (C) 2016 Edward Fauchon-Jones
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import h5py as h5


class Error(object):
    """Base class for error objects describing broken format fields

    Parameters
    ----------
    spec: lvcnrpy.format.specs.Spec
        Field spec to instance an `Error` for.

    Attributes
    ----------
    name: str
        Human readable name given to the error.
    msg: str, optional
        Verbose message describing the specifics of the error instance. Should
        be `None` if no message is required.
    """
    name = "ERROR"
    msg = None

    def __init__(self, spec):
        pass


class Valid(Error):
    """No error"""
    name = "="


class ValidInterfield(Error):
    """No error for interfield"""
    name = "="
    msg = "({0:s})"

    def __init__(self, spec):
        self.msg = self.msg.format(spec.validMsg)


class InvalidValue(Error):
    """Defined value is not a value the field may take"""
    name = "INVALID VALUE"
    msg = "(Value must be one of {0:s})"

    def __init__(self, spec):
        values = ["{0:}".format(v) for v in spec.values]
        values = ", ".join(values)
        self.msg = self.msg.format(values)


class InvalidSubFields(Error):
    """Defined GroupSpec does not have the required subfields

    Parameters
    ----------
    groupSpec: lvcnrpy.format.specs.GroupSpec
        Group field spec to instance an `InvalidSubFields` for.
    sim: lvcnrpy.Sim.Sim
        Simulation being tested that has this error in formating.
    """
    name = "INVALID SUBFIELDS"
    msg = "(Field has subfields [{0:s}] but should have [{1:s}])"

    def __init__(self, groupSpec, sim):
        definedSubFields = ", ".join(sim[groupSpec.name].keys())
        requiredSubFields = ", ".join(groupSpec.subfields)
        self.msg = self.msg.format(definedSubFields, requiredSubFields)


class WrongType(Error):
    """Defined values type is the type the field may take"""
    name = "WRONG TYPE"
    msg = "(Type must be {0:})"

    def __init__(self, spec):
        self.msg = self.msg.format(spec.dtype)


class Missing(Error):
    """No value is defined for the field"""
    name = "MISSING"
    msg = None


class InvalidModeName(Error):
    """Mode's absolute order value exceeds degree"""
    name = "INVALID MODE"
    msg = "(Order {0:} of mode is not allowed for degree {1:})"

    def __init__(self, spec):
        self.msg = self.msg.format(spec.m, spec.l)


class InvalidInterfield(Error):
    """Interfield check has failed"""
    name = "INVALID INTERFIELD"
    msg = "({0:s})"

    def __init__(self, spec):
        self.msg = self.msg.format(spec.invalidMsg)


class InvalidInterfields(Error):
    """Interfield check failed because the field dependencies are invalid"""
    name = "INVALID FIELDS"
    msg = "(Field dependencies are invalid)"


class InvalidSequence(Error):
    """Defined sequence is not valid"""
    name = "INVALID SEQUENCE"
    msg = "({0:s})"

    def __init__(self, spec):
        self.msg = self.msg.format(spec.invalidMsg)
