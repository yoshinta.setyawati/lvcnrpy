# Copyright (C) 2016 Edward Fauchon-Jones
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from h5py import File
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as IUS
from collections import OrderedDict
from os.path import relpath, abspath
from . import LVCNR_DATADIR
from lvcnrpy.format.specs import Spec, GroupSpec, ROMSplineSpec
from lvcnrpy.format.format import format1
import hashlib


class Sim(File):
    """Wrapper to load a single hdf5 NR file

    Parameters
    ----------
    name: str
        Path to the hdf5 NR file to load
    """

    def getAmpIUS(self, l, m):
        """Load amp_lm mode from the hdf5 NR file as a spline

        Parameters
        ----------
        l,m: int
            load mode of degree l and order m. This corresponds to the hdf5
            group `amp_l{l}_m{m}`.

        Returns
        -------
        amp_lm: scipy.interpolate.InterpolatedUnivariateSpline
            Return `amp_lm` as an spline using 5th degree interpolants
        """
        ampPath = 'amp_l{0:d}_m{1:d}'.format(l, m)
        t = self['{0:s}/knots'.format(ampPath)]
        amp = self['{0:s}/data'.format(ampPath)]
        ampIUS = IUS(t, amp, k=5)

        return ampIUS

    def getPhaseIUS(self, l, m):
        """Load phase_lm mode from the hdf5 NR file as a spline

        Parameters
        ----------
        l,m: int
            load mode of degree l and order m. This corresponds to the hdf5
            group `phase_l{l}_m{m}`.

        Returns
        -------
        phase_lm: scipy.interpolate.InterpolatedUnivariateSpline
            Return `phase_lm` as an spline using 5th degree interpolants
        """
        phasePath = 'phase_l{0:d}_m{1:d}'.format(l, m)
        t = self['{0:s}/knots'.format(phasePath)]
        phase = self['{0:s}/data'.format(phasePath)]
        phaseIUS = IUS(t, phase, k=5)

        return phaseIUS

    def att(self, att):
        """Get value of attribute from hdf5 NR file

        Convinience method to functional access attributes of the hdf5 NR file

        Parameters
        ----------
        att: str
            Name of the attribute to get the value of

        Returns
        -------
        value: type(self.attrs[att])
            Value of the attribute with name `att`. The type will match that of
            the stored attribute.
        """
        try:
            return self.attrs[att]
        except:
            return None

    def getAttrs(self):
        """Get attributes of the level 1 format specification

        Returns
        -------
        attrs: OrderedDict of (name: value)
            This returns an ordered dictionary of the level 1 format
            specification attributes. The order is determined by that order
            the fields are presented in the the format specification.
        """
        def getEntry(field):
            attr = self.att(field)
            if isinstance(attr, np.ndarray):
                attr = attr.tolist()
            return (field, attr)

        def getAnnexPath():
            absFilename = abspath(self.filename)
            absLVCNR_DATADIR = abspath(LVCNR_DATADIR)
            if absFilename.startswith(absLVCNR_DATADIR):
                return relpath(absFilename, absLVCNR_DATADIR)
            else:
                return self.filename

        def flatten(container):
            for k, i in container.items():
                if isinstance(i, OrderedDict):
                    for j in flatten(i):
                        yield j
                else:
                    yield i

        # Generate flattened list of attributes
        attrs = flatten(format1)
        attrs = [e for e in attrs if not isinstance(
            e(), (GroupSpec, ROMSplineSpec))]

        # Prepare as key-value pairs
        attrs = [getEntry(e.name) for e in attrs]

        # Add relative (Annex base) path
        attrs.append(("path", getAnnexPath()))

        # Add hash as id for simulation
        sha = hashlib.sha256(open(self.filename, 'rb').read()).hexdigest()
        attrs.append(("sha256", sha))

        return OrderedDict(attrs)
